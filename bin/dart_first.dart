import 'package:dart_first/dart_first.dart' as dart_first;

void main(List<String> arguments) {

  var name = "Syaiful";

  int a = 3;
  int b = 5;
  int result = a + b;
  print('Hello Mas: $name hasil dari $a + $b sama dengan = $result');



  print('');
  print('-----initiation----');
  var myAge;
  myAge = 25;
  print('var myAge;');
  print('myAge = 25;');
  print("My Age is = $myAge");


  print('');
  print('----Variable Dynamic-----');
  var x;
  x = 7;
  x = 'Dart is Great';
  x = 8;
  print('var x');
  print('x = 7');
  print('x = Dart is Great');
  print('x = 8');
  print('variable dynamic of x is $x');


  

}
